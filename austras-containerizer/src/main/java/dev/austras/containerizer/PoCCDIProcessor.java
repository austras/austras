package dev.austras.containerizer;

import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import java.io.IOException;
import java.util.Set;


@SupportedAnnotationTypes("*")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
public class PoCCDIProcessor extends AbstractProcessor {
    private boolean isCreated;

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (roundEnv.processingOver()) {
            return false;
        }
        System.out.println("PROCESSING ANNOTATIONS " + roundEnv.getRootElements());
        if (!isCreated) {
            isCreated = true;
            System.out.println("GENERATING FILE");
            try (var out = processingEnv.getFiler().createSourceFile("guru.bug.cdiproc.app.TestClass").openWriter()) {

                out.write("package guru.bug.cdiproc.app;\n");
                out.write("public class TestClass {\n");
                out.write("    public void sayHello() {\n");
                out.write("        System.out.println(\"Hello!\");\n");
                out.write("    }\n");
                out.write("}\n");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        roundEnv.getRootElements().forEach(this::logMethods);

        return false;
    }

    private void logMethods(Element element) {
        if (!element.getKind().isClass()) {
            System.out.println(element + " isn't a class");
        }

        var type = (TypeElement) element;

        type.getEnclosedElements().stream()
                .filter(e -> e.getKind() == ElementKind.METHOD)
                .forEach(e -> applyLog((ExecutableElement) e));

    }

    private void applyLog(ExecutableElement method) {
        System.out.println("APPLYING LOG TO " + method);

        final Context context = ((JavacProcessingEnvironment) processingEnv).getContext();
        final JavacElements elementUtils = (JavacElements) processingEnv.getElementUtils();
        final TreeMaker treeMaker = TreeMaker.instance(context);
        JCTree.JCMethodDecl jcMethodDecl = (JCTree.JCMethodDecl) elementUtils.getTree(method);

        treeMaker.pos = jcMethodDecl.pos;
        jcMethodDecl.body.stats = List.of(
                printStmt(elementUtils, treeMaker, "STARTING METHOD: " + method),
                treeMaker.Try(
                        treeMaker.Block(0, jcMethodDecl.body.stats),
                        List.nil(),
                        treeMaker.Block(0, List.of(
                                printStmt(elementUtils, treeMaker, "FINISHING METHOD: " + method)
                                )

                        )
                )
        );
    }

    private JCTree.JCExpressionStatement printStmt(JavacElements elementUtils, TreeMaker treeMaker, String msg) {
        return treeMaker.Exec(
                treeMaker.Apply(
                        List.nil(),
                        treeMaker.Select(
                                treeMaker.Select(
                                        treeMaker.Ident(
                                                elementUtils.getName("System")
                                        ),
                                        elementUtils.getName("out")
                                ),
                                elementUtils.getName("println")
                        ),
                        List.of(
                                treeMaker.Literal(msg)
                        )
                )
        );
    }
}
