package dev.austras.containerizer;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.Tree;
import com.sun.source.util.*;
import com.sun.tools.javac.api.BasicJavacTask;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Names;

public class PoCCompilerPlugin implements Plugin {
    private Context ctx;

    @Override
    public String getName() {
        return "PoCCompilerPlugin";
    }

    @Override
    public void init(JavacTask task, String... args) {
        ctx = ((BasicJavacTask) task).getContext();
        System.out.println("COMPILER PLUGIN INIT");
        task.addTaskListener(new TaskListener() {
            @Override
            public void started(TaskEvent e) {
                System.out.println("started: " + e);
                if (e.getKind() == TaskEvent.Kind.ANALYZE) {
//                    addLog(e.getCompilationUnit());
                }
            }

            @Override
            public void finished(TaskEvent e) {
                System.out.println("finished: " + e);
            }
        });
    }

    private void addLog(CompilationUnitTree e) {
        e.accept(new TreeScanner<Void, Void>() {
            @Override
            public Void visitMethod(MethodTree node, Void aVoid) {
                var body = (JCTree.JCBlock) node.getBody();
                if (node.getName().contentEquals("main")) {
                    System.out.println("MAIN METHOD STATS: [" + body.stats + "]");
                    explore(body.stats);
                }
                return super.visitMethod(node, aVoid);
            }
        }, null);
//        var treeMaker = TreeMaker.instance(ctx);
//        var symbolsTable = Names.instance(ctx);
//        var systemName = symbolsTable.fromString(System.class.getName());
//        var systemType = treeMaker.Ident(systemName).type;
//
//        e.accept(new TreeScanner<Void, Void>() {
//            @Override
//            public Void visitMethod(MethodTree node, Void aVoid) {
//                var log = factory.at(((JCTree)node).pos)
//                        .ClassLiteral(systemType)
//
//                treeMaker.Exec(
//                        treeMaker.Apply(
//                                List.<JCTree.JCExpression>nil(),
//                                treeMaker.Select(
//                                        treeMaker.Select(
//                                                treeMaker.Ident(systemName),
//                                                elementUtils.getName("out")
//                                        ),
//                                        elementUtils.getName("println")
//                                ),
//                                List.<JCTree.JCExpression>of(
//                                        treeMaker.Literal("Hello, world!!!")
//                                )
//                        )
//                )
//                var body = (JCTree.JCBlock) node.getBody();
//                body.stats = body.stats.prepend(log);
//                return super.visitMethod(node, aVoid);
//            }
//        }, null);
    }

    private void explore(List<JCTree.JCStatement> stats) {
        for (var s : stats) {
            System.out.println("----------");
            explore(s, 0);
        }
    }

    private void explore(JCTree s, int level) {
        System.out.print("  ".repeat(level));
        print(s);
        s.accept(new TreeScanner<Void, Void>() {
            @Override
            public Void scan(Tree tree, Void aVoid) {
                var t = (JCTree) tree;
                explore(t, level + 1);
                return super.scan(tree, aVoid);
            }
        }, null);
    }

    private void print(JCTree s) {
        System.out.printf("pos: %d; type: %s; class: %s [%s]%n", s.pos, s.type, s.getClass(), s);
    }
}
