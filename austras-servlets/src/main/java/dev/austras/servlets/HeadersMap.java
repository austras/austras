package dev.austras.servlets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

public class HeadersMap {
    private final HashMap<String, Collection<String>> backingMap = new HashMap<>();

    public int size() {
        return backingMap.size();
    }

    public boolean isEmpty() {
        return backingMap.isEmpty();
    }

    public boolean hasHeader(String name) {
        return backingMap.containsKey(name.toUpperCase());
    }

    public Collection<String> getAll(String name) {
        var result = backingMap.get(name.toUpperCase());
        return result == null ? null : Collections.unmodifiableCollection(result);
    }

    public void put(String key, String value) {
        backingMap.computeIfAbsent(key.toUpperCase(), k -> new ArrayList<>(3)).add(value);
    }

    public boolean remove(String name) {
        return backingMap.remove(name.toUpperCase()) != null;
    }

    public void clear() {
        backingMap.clear();
    }

}
