package dev.austras.servlets;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.ForkJoinPool;

public class Poc {
    public static void main(String[] args) throws IOException, InterruptedException {
        var grp = AsynchronousChannelGroup.withThreadPool(ForkJoinPool.commonPool());
        var listener = AsynchronousServerSocketChannel.open(grp).bind(new InetSocketAddress(8080));
        listener.accept(null, new CompletionHandler<AsynchronousSocketChannel, Void>() {
            @Override
            public void completed(AsynchronousSocketChannel channel, Void attachment) {
                listener.accept(null, this);
                handle(channel);
            }

            @Override
            public void failed(Throwable exc, Void attachment) {
                System.out.println("FAILED " + exc);
            }
        });
        Thread.sleep(Long.MAX_VALUE);
    }

    private static void handle(AsynchronousSocketChannel channel) {
        var buf = ByteBuffer.allocate(8192);
        var status = new Object() {
            boolean waitCR = true;
            boolean firstLine = true;
            boolean header = true;
            int count;
            int lastPos;
        };
        channel.read(buf, null, new CompletionHandler<Integer, Void>() {
            @Override
            public void completed(Integer result, Void aVoid) {
                if (result == -1) {
                    print(buf);
                }
                var lastPos = buf.position();
                var arr = buf.array();
                for (int i = status.lastPos; i <= lastPos; i++) {
                    var b = arr[i];
                    if (status.waitCR) {
                        if (b == 13) {
                            status.waitCR = false;
                        }
                    } else if (b == 10) {
                        status.waitCR = true;
                        status.count++;
                        if (status.count == 2) {
                            print(buf);
                        }
                    } else {
                        status.waitCR = true;
                        status.count = 0;
                    }
                }
                status.lastPos = lastPos + 1;


                if (!buf.hasRemaining()) {
                    System.out.println("Overflow");
                    try {
                        channel.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failed(Throwable exc, Void aVoid) {
                System.out.println("Failed " + exc);
            }
        });
    }

    private static void print(ByteBuffer buf) {
        buf.flip();
        var r = new String(buf.array(), 0, buf.remaining());
        System.out.println(r);
    }

}
