package dev.austras.servlets.datetime;

public class HttpDateTimeParseException extends Exception {

    public HttpDateTimeParseException(String msg) {
        super("Timestamp parsing exception: " + msg);
    }

    public HttpDateTimeParseException(int idx) {
        super("at index " + idx);
    }

    public HttpDateTimeParseException(String field, int idx) {
        super(field + " at index " + idx);
    }

    public HttpDateTimeParseException(String field, int idx, Throwable cause) {
        super(field + " at index " + idx, cause);
    }

}
