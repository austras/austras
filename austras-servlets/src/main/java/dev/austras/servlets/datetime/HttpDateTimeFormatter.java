package dev.austras.servlets.datetime;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class HttpDateTimeFormatter {

    public static long parse(String str) throws HttpDateTimeParseException {
        var chars = str.toCharArray();
        int length = chars.length;
        if (length == 29) {
            return parseRFC1123(chars);
        } else if (length == 24) {
            return parseASCTime(chars);
        } else if (length >= 30 && length <= 33) {
            return parseRFC850(chars);
        } else {
            throw new HttpDateTimeParseException("Unsupported timestamp length " + length);
        }
    }

    /*
            00000000001111111111222222222
            01234567890123456789012345678
            Sun, 06 Nov 1994 08:49:37 GMT -- length 29
     */
    private static long parseRFC1123(char[] chars) throws HttpDateTimeParseException {
        var dow = parseDayOfWeekThreeChars(chars, 0);
        ensureLiteral(chars, 3, ',', ' ');
        var dom = parseDayOfMonthTwoDigitsZeroPadded(chars, 5);
        ensureLiteral(chars, 7, ' ');
        var mon = parseMonthOfYearThreeChars(chars, 8);
        ensureLiteral(chars, 11, ' ');
        var yer = parseYearFourDigits(chars, 12);
        ensureLiteral(chars, 16, ' ');
        var hrs = parseHourOfDayTwoDigitsZeroPadded(chars, 17);
        ensureLiteral(chars, 19, ':');
        var min = parseMinutesOfHourTwoDigitsZeroPadded(chars, 20);
        ensureLiteral(chars, 22, ':');
        var sec = parseSecondsOfMinuteTwoDigitsZeroPadded(chars, 23);
        ensureGMTLiteral(chars, 25);

        return timestampAsLong(yer, mon, dom, dow, hrs, min, sec);
    }

    /*
         0  0     222221111111111000000000
         0  0     432109876543210987654321
            Sunday, 06-Nov-94 08:49:37 GMT -- length 30 (shortest)
         Wednesday, 06-Nov-94 08:49:37 GMT -- length 33 (longest)
     */
    private static long parseRFC850(char[] chars) throws HttpDateTimeParseException {
        var len = chars.length;
        var dow = parseDayOfWeekFullName(chars, 0);
        ensureLiteral(chars, len - 24, ',', ' ');
        var dom = parseDayOfMonthTwoDigitsZeroPadded(chars, len - 22);
        ensureLiteral(chars, len - 20, '-');
        var mon = parseMonthOfYearThreeChars(chars, len - 19);
        ensureLiteral(chars, len - 16, '-');
        var yer = parseYearTwoDigits(chars, len - 15);
        ensureLiteral(chars, len - 13, ' ');
        var hrs = parseHourOfDayTwoDigitsZeroPadded(chars, len - 12);
        ensureLiteral(chars, len - 10, ':');
        var min = parseMinutesOfHourTwoDigitsZeroPadded(chars, len - 9);
        ensureLiteral(chars, len - 7, ':');
        var sec = parseSecondsOfMinuteTwoDigitsZeroPadded(chars, len - 6);
        ensureGMTLiteral(chars, len - 4);

        return timestampAsLong(yer, mon, dom, dow, hrs, min, sec);
    }

    /*
            000000000011111111112222
            012345678901234567890123
            Sun Nov  6 08:49:37 1994 -- length 24
     */
    private static long parseASCTime(char[] chars) throws HttpDateTimeParseException {
        var dow = parseDayOfWeekThreeChars(chars, 0);
        ensureLiteral(chars, 3, ' ');
        var mon = parseMonthOfYearThreeChars(chars, 4);
        ensureLiteral(chars, 7, ' ');
        var dom = parseDayOfMonthTwoDigitsSpacePadded(chars, 8);
        ensureLiteral(chars, 10, ' ');
        var hrs = parseHourOfDayTwoDigitsZeroPadded(chars, 11);
        ensureLiteral(chars, 13, ':');
        var min = parseMinutesOfHourTwoDigitsZeroPadded(chars, 14);
        ensureLiteral(chars, 15, ':');
        var sec = parseSecondsOfMinuteTwoDigitsZeroPadded(chars, 17);
        ensureLiteral(chars, 19, ' ');
        var yer = parseYearFourDigits(chars, 20);

        return timestampAsLong(yer, mon, dom, dow, hrs, min, sec);
    }

    private static int parseDayOfWeekThreeChars(char[] chars, int idx) throws HttpDateTimeParseException {
        var c0 = chars[idx];
        var c1 = chars[idx + 1];
        var c2 = chars[idx + 2];
        switch (c0) {
            case 'M':
                if (c1 == 'o' && c2 == 'n') {
                    return 1; // Mon
                }
                break;
            case 'T':
                switch (c1) {
                    case 'u':
                        if (c2 == 'e') return 2; // Tue
                        break;
                    case 'h':
                        if (c2 == 'u') return 4; // Thu
                        break;
                }
                break;
            case 'W':
                if (c1 == 'e' && c2 == 'd') {
                    return 3; // Wed
                }
                break;
            case 'F':
                if (c1 == 'r' && c2 == 'i') {
                    return 5; // Fri
                }
                break;
            case 'S':
                switch (c1) {
                    case 'a':
                        if (c2 == 't') return 6; // Sat
                        break;
                    case 'u':
                        if (c2 == 'n') return 7; // Sun
                        break;
                }
                break;
        }
        throw new HttpDateTimeParseException("day of week", idx);
    }

    private static int parseDayOfWeekFullName(char[] chars, int idx) throws HttpDateTimeParseException {
        var c0 = chars[idx];
        var c1 = chars[idx + 1];
        var c2 = chars[idx + 2];
        var c3 = chars[idx + 3];
        var c4 = chars[idx + 4];
        var c5 = chars[idx + 5];
        switch (c0) {
            case 'M':
                if (c1 == 'o' && c2 == 'n' && isDayLiteral(chars, idx + 3)) return 1; // Mon
                break;
            case 'T':
                if (c1 == 'u') {
                    if (c2 == 'e' && c3 == 's' && isDayLiteral(chars, idx + 4)) return 2; // Tue
                } else if (c1 == 'h' && c2 == 'u' && c3 == 'r' && c4 == 's' && isDayLiteral(chars, idx + 5)) return 4; // Thu
                break;
            case 'W':
                if (c1 == 'e' && c2 == 'd' && c3 == 'n' && c4 == 'e' && c5 == 's' && isDayLiteral(chars, idx + 6)) return 3; // Wed
                break;
            case 'F':
                if (c1 == 'r' && c2 == 'i' && isDayLiteral(chars, idx + 3)) return 5; // Fri
                break;
            case 'S':
                if (c1 == 'a') {
                    if (c2 == 't' && c3 == 'u' && c4 == 'r' && isDayLiteral(chars, idx + 5)) return 6; // Sat
                } else if (c1 == 'u' && c2 == 'n' && isDayLiteral(chars, idx + 3)) return 7; // Sun
                break;
        }
        throw new HttpDateTimeParseException("day of week", idx);
    }

    private static boolean isDayLiteral(char[] chars, int idx) {
        return chars[idx] == 'd' && chars[idx + 1] == 'a' && chars[idx + 2] == 'y';
    }

    private static int parseDayOfMonthTwoDigitsZeroPadded(char[] chars, int idx) throws HttpDateTimeParseException {
        try {
            var hi = parseDigit(chars, idx);
            return continueParseDayOfMonth(chars, idx, hi);
        } catch (HttpDateTimeParseException e) {
            throw new HttpDateTimeParseException("day of month", idx, e);
        }
    }

    private static int parseDayOfMonthTwoDigitsSpacePadded(char[] chars, int idx) throws HttpDateTimeParseException {
        try {
            var hi = chars[idx] == ' ' ? 0 : parseDigit(chars, idx);
            return continueParseDayOfMonth(chars, idx, hi);
        } catch (HttpDateTimeParseException e) {
            throw new HttpDateTimeParseException("day of month", idx, e);
        }
    }

    private static int continueParseDayOfMonth(char[] chars, int idx, int hi) throws HttpDateTimeParseException {
        if (hi > 3) {
            throw new HttpDateTimeParseException(idx);
        }
        var lo = parseDigit(chars, idx + 1);
        if (hi == 3 && lo > 1) {
            throw new HttpDateTimeParseException(idx + 1);
        }
        return hi * 10 + lo;
    }

    private static int parseMonthOfYearThreeChars(char[] chars, int idx) throws HttpDateTimeParseException {
        var c0 = chars[idx];
        var c1 = chars[idx + 1];
        var c2 = chars[idx + 2];
        switch (c0) {
            case 'J':
                if (c1 == 'a') {
                    if (c2 == 'n') return 1; // jan
                } else if (c1 == 'u') {
                    if (c2 == 'n') return 6; // jun
                    if (c2 == 'l') return 7; // jul
                }
                break;
            case 'F':
                if (c1 == 'e' && c2 == 'b') return 2; // feb
                break;
            case 'M':
                if (c1 == 'a') {
                    if (c2 == 'r') return 3; // mar
                    if (c2 == 'y') return 5; // may
                }
                break;
            case 'A':
                if (c1 == 'p') {
                    if (c2 == 'r') return 4; // apr
                } else if (c1 == 'u' && c2 == 'g') return 8; // aug
                break;
            case 'S':
                if (c1 == 'e' && c2 == 'p') return 9; // sep
                break;
            case 'O':
                if (c1 == 'c' && c2 == 't') return 10; // oct
                break;
            case 'N':
                if (c1 == 'o' && c2 == 'v') return 11; // nov
                break;
            case 'D':
                if (c1 == 'e' && c2 == 'c') return 12; // dec
                break;
        }
        throw new HttpDateTimeParseException("month", idx);
    }

    private static int parseYearFourDigits(char[] chars, int idx) throws HttpDateTimeParseException {
        try {
            var d0 = parseDigit(chars, idx);
            var d1 = parseDigit(chars, idx + 1);
            var d2 = parseDigit(chars, idx + 2);
            var d3 = parseDigit(chars, idx + 3);

            return d0 * 1000 + d1 * 100 + d2 * 10 + d3;
        } catch (HttpDateTimeParseException e) {
            throw new HttpDateTimeParseException("year", idx, e);
        }
    }

    private static int parseYearTwoDigits(char[] chars, int idx) throws HttpDateTimeParseException {
        try {
            // this is weird logic, but I don't know better approach to convert 2-digits year to 4-digits
            var d0 = parseDigit(chars, idx);
            var d1 = parseDigit(chars, idx + 1);
            var tmp = d0 * 10 + d1;
            if (tmp > 50) {
                return 1900 + tmp;
            } else {
                return 2000 + tmp;
            }
        } catch (HttpDateTimeParseException e) {
            throw new HttpDateTimeParseException("2 digits year", idx, e);
        }
    }

    private static int parseHourOfDayTwoDigitsZeroPadded(char[] chars, int idx) throws HttpDateTimeParseException {
        try {
            var hi = parseDigit(chars, idx);
            if (hi > 2) {
                throw new HttpDateTimeParseException(idx);
            }
            var lo = parseDigit(chars, idx + 1);
            if (hi == 2 && lo > 3) {
                throw new HttpDateTimeParseException(idx + 1);
            }
            return hi * 10 + lo;
        } catch (HttpDateTimeParseException e) {
            throw new HttpDateTimeParseException("hours", idx, e);
        }
    }

    private static int parseMinutesOfHourTwoDigitsZeroPadded(char[] chars, int idx) throws HttpDateTimeParseException {
        try {
            return parseClock(chars, idx);
        } catch (HttpDateTimeParseException e) {
            throw new HttpDateTimeParseException("hours", idx, e);
        }
    }

    private static int parseSecondsOfMinuteTwoDigitsZeroPadded(char[] chars, int idx) throws HttpDateTimeParseException {
        try {
            return parseClock(chars, idx);
        } catch (HttpDateTimeParseException e) {
            throw new HttpDateTimeParseException("minutes", idx, e);
        }
    }

    private static int parseClock(char[] chars, int idx) throws HttpDateTimeParseException {
        var hi = parseDigit(chars, idx);
        if (hi > 5) {
            throw new HttpDateTimeParseException(idx);
        }
        var lo = parseDigit(chars, idx + 1);
        return hi * 10 + lo;
    }

    private static int parseDigit(char[] chars, int idx) throws HttpDateTimeParseException {
        var ch = chars[idx];
        if (ch < '0' || ch > '9') {
            throw new HttpDateTimeParseException("digit", idx);
        }
        return ch - '0';
    }

    private static void ensureLiteral(char[] chars, int idx, char c) throws HttpDateTimeParseException {
        if (chars[idx] != c) {
            throw new HttpDateTimeParseException("" + c, idx);
        }
    }

    private static void ensureLiteral(char[] chars, int idx, char c0, char c1) throws HttpDateTimeParseException {
        if (chars[idx] != c0 || chars[idx + 1] != c1) {
            throw new HttpDateTimeParseException("" + c0 + c1, idx);
        }
    }

    private static void ensureGMTLiteral(char[] chars, int idx) throws HttpDateTimeParseException {
        if (chars[idx] != ' ' || chars[idx + 1] != 'G' || chars[idx + 2] != 'M' || chars[idx + 3] != 'T') {
            throw new HttpDateTimeParseException("GMT", idx);
        }
    }

    private static long timestampAsLong(int yer, int mon, int dom, int dow, int hrs, int min, int sec) throws HttpDateTimeParseException {
        var locDT = LocalDateTime.of(yer, mon, dom, hrs, min, sec);
        if (locDT.getDayOfWeek().getValue() != dow) {
            throw new HttpDateTimeParseException("Invalid timestamp: " + locDT + " isn't " + DayOfWeek.of(dow));
        }
        return locDT.toInstant(ZoneOffset.UTC).toEpochMilli();
    }
}
