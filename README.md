                                      /\      /\
                                     /  \    /  \         
                                    /    \  /    \ 
                                    \     \/     /   
                                     \          /        
                                 /\   \        /   /\ 
                                /  \   \      /   /  \
                               /    \  /      \  /    \
                               \     \/        \/     /
                                \                    /
                            /\   \        /\        /   /\
                           /  \   \      /  \      /   /  \
                          /    \  /      \  /      \  /    \
                          \     \/        \/        \/     /
                           \                              / 
                            \        /\        /\        /
                             \      /  \      /  \      /
                              \    /    \    /    \    /
                               \  /      \  /      \  /
                                \/        \/        \/ 
                                
                         ============== AUSTRAS ============= 
                        
# Austras - lightweight Eclipse Microprofile container generator

Austras is a quick and minimalistic framework  

* **Standards-based**:
Based on [Eclipse Microprofile](https://microprofile.io/) and [Jakarta EE](https://jakarta.ee/)
* **Microservices-oriented**:
Small memory footprint and quick start makes your application a perfect microservice
* **Containers-minded**:
Executable jar and flexible configuration great to run in containers
* **Developers-centric**:
Everything is happening during build time. So most errors are reported very soon 